package jsoncache_test

import (
	"fmt"
	"os"
	"time"
	//	"testing"

	"github.com/robmuh/go-jsoncache"
)

func ExampleNew() {
	jc := jsoncache.New()

	fmt.Println(jc.Data)

	dir := jc.Dir
	fmt.Println(dir[len(dir)-19:])

	fmt.Println(jc.File)

	path := jc.Path()
	fmt.Println(path[len(path)-30:])

	jc.File = "anothername.json"
	newpath := jc.Path()
	fmt.Println(newpath[len(newpath)-36:])

	// Output:
	// {}
	// /.go-jsoncache.test
	// cache.json
	// /.go-jsoncache.test/cache.json
	// /.go-jsoncache.test/anothername.json
}

func ExampleSet() {
	jc := jsoncache.New()
	jc.Set("name", "Mr. Rob")
	fmt.Println(jc.Data)
	// Output:
	// {"name":"Mr. Rob"}
}

func ExampleGet() {
	jc := jsoncache.New()
	jc.Set("name", "Mr. Rob")
	name := jc.Get("name")
	fmt.Println(name)
	// Output:
	// Mr. Rob
}

func ExampleGet_empty() {
	jc := jsoncache.New()
	name := jc.Get("name")
	fmt.Println(name)
	// Output:
	// <nil>
}

func ExampleNewFromJSON() {
	jsn := `{"data":{"name": "Mr. Rob"}}`
	jc, _ := jsoncache.NewFromJSON([]byte(jsn))
	name := jc.Get("name")
	fmt.Println(name)
	// Output:
	// Mr. Rob
}

func ExampleNewFromFile() {
	jc, _ := jsoncache.NewFromFile("testing/mapsample.json")
	name := jc.Get("name")
	fmt.Println(name)
	// Output:
	// Mr. Rob
}

func ExampleLoad() {
	jc := jsoncache.New()

	jc.Set("Some", "Thing")
	fmt.Println(jc.Data)

	jc.Dir = "testing"
	jc.File = "mapsample.json"

	jc.Load()
	fmt.Println(jc.Data)

	// Output:
	// {"Some":"Thing"}
	// {"name":"Mr. Rob"}
}

func ExampleSave() {
	jc := jsoncache.New()

	jc.Set("name", "Mr. Rob")
	jc.Dir = "testing"
	jc.File = "saved.json"
	jc.Save()

	// simulate something else updating the file
	now := time.Now()
	os.Chtimes(jc.Path(), now, now)

	err := jc.Save()
	fmt.Println(err)

	err = jc.ForceSave()
	fmt.Println(err)

	// Output:
	// Newer cache file detected.
	// <nil>
}

func ExampleSave_autosave() {
	jc := jsoncache.New()

	jc.Set("name", "Mr. Rob")
	jc.Dir = "testing"
	jc.File = "autosaved.json"
	jc.Every = "3s"

	// check immediately that file does not exist
	if _, err := os.Stat(jc.Path()); os.IsNotExist(err) {
		fmt.Println("Nope, not there.")
	}

	time.Sleep(5 * time.Second)

	// should be there now
	if _, err := os.Stat(jc.Path()); err == nil || !os.IsNotExist(err) {
		fmt.Println("Yep, there it is.")
	}

	// clean up
	os.Remove(jc.Path())

	// Output:
	// Nope, not there.
	// Yep, there it is.

}

/*

func BenchmarkSet(b *testing.B) {
	jc := jsoncache.New()
	for n := 0; n < b.N; n++ {
		jc.Set("name", "Mr. Rob")
	}
}
*/
