JSON Cache
==========

For those times when a database is overkill but you still need reliable,
structured, web-friendly persistence.

Status
------

Usable, tested, documentation coming (like so much else in life).

Motivation
----------

You know how a vim session intelligently notices when the file has
changed while you are editing it and warns you before you force the
save? Well, what if you could plug that functionality into any Go
command or app and have it just do the right thing without much hassle.
JSON Cache gives you this.
