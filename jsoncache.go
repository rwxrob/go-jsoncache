package jsoncache

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"sync"
	"time"
)

var NewerCacheError = errors.New("Newer cache file detected.")

type Data map[string]interface{}

func (d Data) String() string {
	byt, err := json.Marshal(d)
	if err != nil {
		return fmt.Sprintf("{\"ERROR\":\"%v\"}", err)
	}
	return string(byt)
}

type Cache struct {
	mu      sync.Mutex
	unsaved bool

	// Dir defaults to a full path to a home directory named after
	// the running executable with a dot in front ("~/.myproggy").
	Dir string `json:"dir,omitempty"`

	// File name defaults to "cache.json", which will be placed into the
	// Dir directory.
	File string `json:"file,omitempty"`

	// Data is the actual key/value data.
	Data Data `json:"data"`

	// Save is set to the last time a save successfully completed.
	Saved time.Time `json:"saved,omitempty"`

	// Every indicates the duration interval for automatic Saves (if any).
	Every string `json:"every,omitempty"`
}

func (jc *Cache) Path() string {
	return path.Join(jc.Dir, jc.File)
}

// New accepts up to three variadic arguments and returns a new Cache
// pointer. First argument is the Dir path string. Second is File name.
// Third is a parsable time.Duration string for the interval of Every
// automatic Save.
func New(args ...string) *Cache {
	jc := new(Cache)
	jc.Data = map[string]interface{}{}
	jc.Every = "0s" // default

	if len(args) > 0 {
		jc.Dir = args[0]
	} else {
		jc.Dir = HomeDotDir() // default
	}

	if len(args) > 1 {
		jc.File = args[1]
	} else {
		jc.File = "cache.json" // default
	}

	if len(args) > 2 {
		_, err := time.ParseDuration(args[2])
		if err != nil {
			panic(err)
		}
		jc.Every = args[2]
	}

	go jc.autosave()

	return jc
}

func NewFromJSON(jsn []byte, args ...string) (*Cache, error) {
	jc := New(args...)
	err := json.Unmarshal(jsn, &jc)
	return jc, err
}

// NewFromFile uses the specific path to read a json file. However, the
// path does not automatically set the Dir and File, which must be
// provided as separate args as well if the default initial values are
// not wanted.
func NewFromFile(path string, args ...string) (*Cache, error) {
	byt, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return NewFromJSON(byt, args...)
}

// TODO:
// func Import
// func ImportFile
// func ImportJSON

func (jc *Cache) Get(key string) interface{} {
	jc.mu.Lock()
	defer jc.mu.Unlock()
	return jc.Data[key]
}

func (jc *Cache) Set(key string, val interface{}) {
	jc.mu.Lock()
	defer jc.mu.Unlock()
	jc.Data[key] = val
	jc.unsaved = true
}

// Load initializes the Cache object with data freshly loaded from the
// current Path() throwing away the reference to any previous Data
// (which will be cleaned up with normal garbage collection).
func (jc *Cache) Load() error {
	newjc, err := NewFromFile(jc.Path())
	if err != nil {
		return err
	}
	jc.Data = newjc.Data
	return nil
}

// autosave is always started for every new cache but does nothing
// unless the Every duration is set to something other than zero, which
// it checks for every second.
func (jc *Cache) autosave() {
	for {
		dur, err := time.ParseDuration(jc.Every)
		if err != nil {
			panic(err)
		}
		if dur > 0 {
			time.Sleep(dur)
			err := jc.Save()
			if err != nil {
				log.Println(err)
				jc.Every = ""
			}
		} else {
			time.Sleep(1 * time.Second)
		}
	}
}

// Save checks the last modified time of the file and refuses to
// overwrite it if the Saved time is older and returns
// a NewerCacheError.
func (jc *Cache) Save() error {
	jc.mu.Lock()
	defer jc.mu.Unlock()
	i, err := os.Stat(jc.Path())
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if !jc.Saved.IsZero() && i.ModTime().After(jc.Saved) {
		return NewerCacheError
	}

	err = ioutil.WriteFile(jc.Path(), []byte(jc.String()), 0600)
	if err != nil {
		return err
	}

	i, err = os.Stat(jc.Path()) // better than time.Now()
	jc.Saved = i.ModTime()
	jc.unsaved = false
	return nil
}

func (jc *Cache) ForceSave() error {
	jc.mu.Lock()
	defer jc.mu.Unlock()
	return ioutil.WriteFile(jc.Path(), []byte(jc.String()), 0600)
}

func (jc *Cache) String() string {
	byt, err := json.Marshal(jc)
	if err != nil {
		return fmt.Sprintf("{\"ERROR\":\"%v\"}", err)
	}
	return string(byt)
}

func HomeDotDir() string {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}
	return path.Join(usr.HomeDir, "."+path.Base(os.Args[0]))
}
